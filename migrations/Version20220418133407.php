<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220418133407 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE degree_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE education (id INT AUTO_INCREMENT NOT NULL, personal_data_id INT NOT NULL, type_id INT NOT NULL, degree_name VARCHAR(255) NOT NULL, graduation_date DATE NOT NULL, INDEX IDX_DB0A5ED2B4D2A817 (personal_data_id), INDEX IDX_DB0A5ED2C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_experience (id INT AUTO_INCREMENT NOT NULL, personal_data_id INT NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, city VARCHAR(255) NOT NULL, company VARCHAR(255) NOT NULL, is_employed TINYINT(1) NOT NULL, INDEX IDX_6FE7511EB4D2A817 (personal_data_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE personal_data (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, profession VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, visible TINYINT(1) NOT NULL, birthday DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social_links (id INT AUTO_INCREMENT NOT NULL, personal_data_id INT NOT NULL, link VARCHAR(255) NOT NULL, INDEX IDX_9B12158AB4D2A817 (personal_data_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE education ADD CONSTRAINT FK_DB0A5ED2B4D2A817 FOREIGN KEY (personal_data_id) REFERENCES personal_data (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE education ADD CONSTRAINT FK_DB0A5ED2C54C8C93 FOREIGN KEY (type_id) REFERENCES degree_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_experience ADD CONSTRAINT FK_6FE7511EB4D2A817 FOREIGN KEY (personal_data_id) REFERENCES personal_data (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE social_links ADD CONSTRAINT FK_9B12158AB4D2A817 FOREIGN KEY (personal_data_id) REFERENCES personal_data (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE education DROP FOREIGN KEY FK_DB0A5ED2C54C8C93');
        $this->addSql('ALTER TABLE education DROP FOREIGN KEY FK_DB0A5ED2B4D2A817');
        $this->addSql('ALTER TABLE job_experience DROP FOREIGN KEY FK_6FE7511EB4D2A817');
        $this->addSql('ALTER TABLE social_links DROP FOREIGN KEY FK_9B12158AB4D2A817');
        $this->addSql('DROP TABLE degree_type');
        $this->addSql('DROP TABLE education');
        $this->addSql('DROP TABLE job_experience');
        $this->addSql('DROP TABLE personal_data');
        $this->addSql('DROP TABLE social_links');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
