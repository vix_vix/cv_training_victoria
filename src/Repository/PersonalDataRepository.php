<?php

namespace App\Repository;

use App\Entity\Education;
use App\Entity\JobExperience;
use App\Entity\PersonalData;
use App\Entity\SocialLinks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Flex\Response;

/**
 * @method PersonalData|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonalData|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonalData[]    findAll()
 * @method PersonalData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonalDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonalData::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(PersonalData $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(PersonalData $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param array $data
     * @param array $links
     * @param array $degrees
     * @param array $experiences
     * @return int|null
     */
    public function addPersonalData(array $data, array $links, array $degrees, array $experiences): ?int
    {
        $personalData = new PersonalData();

        $personalData->setFirstName($data['first_name']);
        $personalData->setLastName($data['last_name']);
        $personalData->setEmail($data['email']);
        $personalData->setPhone($data['phone']);
        $personalData->setAdress($data['address']);
        $personalData->setAbout($data['about']);
        $personalData->setProfession($data['profession']);
        $personalData->setActive($data['active']);
        $personalData->setVisible($data['visible']);
        $personalData->setBirthday(\DateTime::createFromFormat('d.m.Y', $data['birthday']));

        foreach ($experiences as $experience){
            $personalData->addJobExperience($experience);
            $this->_em->persist($experience);
        }

        foreach ($links as $link){
            $personalData->addSocialLink($link);
            $this->_em->persist($link);
        }

        foreach ($degrees as $degree){
            $personalData->addEducation($degree);
            $this->_em->persist($degree);
        }

        $this->_em->persist($personalData);
        $this->_em->flush();

        return $personalData->getId();
    }

    /**
     * @param array $data
     * @param array $links
     * @param array $degrees
     * @param array $experiences
     * @param int $id
     * @return int|null
     */
    public function updatePersonalData(array $data, array $links, array $degrees, array $experiences, int $id): ?int
    {
        $personalData = $this->_em->getRepository(PersonalData::class)->find($id);

        if ($personalData){
            $personalData->setFirstName($data['first_name']);
            $personalData->setLastName($data['last_name']);
            $personalData->setEmail($data['email']);
            $personalData->setPhone($data['phone']);
            $personalData->setAdress($data['address']);
            $personalData->setProfession($data['profession']);
            $personalData->setActive($data['active']);
            $personalData->setVisible($data['visible']);
            $personalData->setBirthday(\DateTime::createFromFormat('d.m.Y', $data['birthday']));

            foreach ($experiences as $experience){
                $personalData->addJobExperience($experience);
                $this->_em->persist($experience);
            }

            foreach ($links as $link){
                $personalData->addSocialLink($link);
                $this->_em->persist($link);
            }

            foreach ($degrees as $degree){
                $personalData->addEducation($degree);
                $this->_em->persist($degree);
            }
            $this->_em->flush();
        }


        return $personalData->getId();
    }

    public function searchAllPersonalData(ManagerRegistry $entityManager): array
    {
        $persons = $this->_em->getRepository(PersonalData::class)->findAll();

        $socialLinksRepository = $entityManager->getRepository(SocialLinks::class);
        $jobExperienceRepository = $entityManager->getRepository(JobExperience::class);
        $educationRepository = $entityManager->getRepository(Education::class);

        $data = [];

        foreach ($persons as $person) {
            $data[] = [
                'personal_data' => ['id' => $person->getId(),
                    'first_name' => $person->getFirstName(),
                    'last_name' => $person->getLastName(),
                    'email' => $person->getEmail(),
                    'phone' => $person->getPhone(),
                    'address' => $person->getAdress(),
                    'profession' => $person->getProfession(),
                    'about' => $person->getAbout(),
                    'active' => $person->getActive(),
                    'visible' => $person->getVisible(),
                    'birthday' => $person->getBirthday()
                ],
                'social_links' => $socialLinksRepository->searchALlSocialLinks($person->getSocialLinks()->getValues()),
                'job_experience' => $jobExperienceRepository->searchAllJobExperience($person->getJobExperiences()->getValues()),
                'education' => $educationRepository->searchAllEducations($person->getEducation()->getValues())

            ];

        }

        return $data;
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function searchOnePersonalData(int $id, ManagerRegistry $entityManager): ?array
    {
        if(!$id){
            throw new NotFoundHttpException('Id is null');
        }

        $person = $this->_em->getRepository(PersonalData::class)->find($id);

        if(!$person){
            throw new NotFoundHttpException('Person not found');
        }

        $personalData = [
            'id' => $person->getId(),
            'first_name' => $person->getFirstName(),
            'last_name' => $person->getLastName(),
            'email' => $person->getEmail(),
            'phone' => $person->getPhone(),
            'address' => $person->getAdress(),
            'profession' => $person->getProfession(),
            'about' => $person->getAbout(),
            'active' => $person->getActive(),
            'visible' => $person->getVisible(),
            'birthday' => $person->getBirthday()

        ];

        $socialLinksRepository = new SocialLinksRepository($entityManager);
        $socialLinks = $socialLinksRepository->getSocialLinks($person->getSocialLinks()->getValues());

        $jobExperienceRepository = new JobExperienceRepository($entityManager);
        $jobExperiences = $jobExperienceRepository->getJobExperiences($person->getJobExperiences()->getValues());

        $educationRepository = new EducationRepository($entityManager);
        $educations = $educationRepository->getEducations($person->getEducation()->getValues());

        return [
            'personal_data' => $personalData,
            'social_links' => $socialLinks,
            'job_experience' => $jobExperiences,
            'education' => $educations
        ];
    }

    public function searchOneQueryBuilder(int $id): array
    {
        $queryBuilder = $this->createQueryBuilder('pd');

        return $queryBuilder->select('pd.id, pd.firstName as first_name, pd.lastName as last_name, pd.email, pd.adress, pd.about,
            pd.profession, pd.active, pd.visible, pd.birthday, sl.id as id_social_link, sl.link, je.id as id_job_experience, je.startDate as start_date,
            je.endDate as end_date, je.city, je.company, je.isEmployed as is_employed')
            ->innerJoin(SocialLinks::class, 'sl', Join::WITH, 'sl.personalData = pd.id')
            ->innerJoin(JobExperience::class, 'je', Join::WITH, 'je.personalData = pd.id')
            ->where("pd.id = $id")
            ->getQuery()
            ->getResult();
    }

}
