<?php

namespace App\Repository;

use App\Entity\DegreeType;
use App\Entity\Education;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;


/**
 * @method Education|null find($id, $lockMode = null, $lockVersion = null)
 * @method Education|null findOneBy(array $criteria, array $orderBy = null)
 * @method Education[]    findAll()
 * @method Education[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EducationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Education::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Education $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Education $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function addEducation(array $degrees): array
    {
        $result = [];

        foreach ($degrees as $degree){
            $education = new Education();
            $education->setDegreeName($degree['degree_name']);
            $education->setGraduationDate(\DateTime::createFromFormat('d.m.Y', $degree['graduation_date']));
            $degreeType = $this->_em->getRepository(DegreeType::class)->find($degree['id_type']);
            $education->setType($degreeType);

            array_push($result, $education);
        }


        return $result;
    }

    public function updateEducation(array $degrees): array
    {
        $result = [];

        foreach ($degrees as $degree){
            if (isset($degree['id'])){
                $education = $this->_em->getRepository(Education::class)->find($degree['id']);
                $education->setDegreeName($degree['degree_name']);
                $education->setGraduationDate(\DateTime::createFromFormat('d.m.Y', $degree['graduation_date']));
                $degreeType = $this->_em->getRepository(DegreeType::class)->find($degree['id_type']);
                $education->setType($degreeType);
            }else{
                $education = new Education();
                $education->setDegreeName($degree['degree_name']);
                $education->setGraduationDate(\DateTime::createFromFormat('d.m.Y', $degree['graduation_date']));
                $degreeType = $this->_em->getRepository(DegreeType::class)->find($degree['id_type']);
                $education->setType($degreeType);
            }
            $result[] = $education;
        }


        return $result;
    }

    public function searchAllEducations(array $educations): int
    {
        return count($educations);
    }

    /**
     * searchOneApi
     * @return array
     */
    public function getEducations(array $degrees): array
    {
        $data = [];

        foreach ($degrees as $degree){
            $data[] = [
                'id' => $degree->getId(),
                'id_type' => $degree->getType()->getId(),
                'faculty' => $degree->getFaculty(),
                'speciality' => $degree->getSpeciality(),
                'degree_name' => $degree->getDegreeName(),
                'graduation_date' => $degree->getGraduationDate()->format('Y')
            ];
        }

        return $data;
    }
}
