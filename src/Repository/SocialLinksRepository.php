<?php

namespace App\Repository;

use App\Entity\Education;
use App\Entity\PersonalData;
use App\Entity\SocialLinks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method SocialLinks|null find($id, $lockMode = null, $lockVersion = null)
 * @method SocialLinks|null findOneBy(array $criteria, array $orderBy = null)
 * @method SocialLinks[]    findAll()
 * @method SocialLinks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocialLinksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SocialLinks::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(SocialLinks $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(SocialLinks $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function addSocialLinks(array $links): array
    {
        $result = [];

        foreach ($links as $link){
            $socialLinks = new SocialLinks();
            $socialLinks->setLink($link);

            $result[] = $socialLinks;
    }

        return $result;
    }

    public function updateSocialLinks(array $links): array
    {
        $result = [];
        foreach ($links as $link){
            if (isset($link['id'])){
                if($socialLinks = $this->_em->getRepository(SocialLinks::class)->find($link['id'])){
                    $socialLinks->setLink($link['link']);
                }
            }else{
                $socialLinks = new SocialLinks();
                $socialLinks->setLink($link['link']);
            }
            $result[] = $socialLinks;
        }
        return $result;
    }

    public function searchALlSocialLinks($links): int
    {
        return count($links);
    }

    public function getSocialLinks(array $links): array
    {
        $data = [];

        foreach ($links as $link){
            $data[] = [
                'id' => $link->getId(),
                'link' => $link->getLink(),
                'type' => $link->getType()
            ];
        }

        return $data;
    }
}
