<?php

namespace App\Repository;

use App\Entity\DegreeType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DegreeType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DegreeType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DegreeType[]    findAll()
 * @method DegreeType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DegreeTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DegreeType::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(DegreeType $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(DegreeType $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
}
