<?php

namespace App\Repository;

use App\Entity\JobExperience;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;


/**
 * @method JobExperience|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobExperience|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobExperience[]    findAll()
 * @method JobExperience[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobExperienceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobExperience::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(JobExperience $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(JobExperience $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function addJobExperience(array $experiences): array
    {
        $result = [];

        foreach ($experiences as $experience){

            $jobExperience = new JobExperience();
            $jobExperience->setStartDate(\DateTime::createFromFormat('d.m.Y', $experience['start_date']));
            $jobExperience->setEndDate(\DateTime::createFromFormat('d.m.Y', $experience['end_date']));
            $jobExperience->setCity($experience['city']);
            $jobExperience->setCompany($experience['company']);
            $jobExperience->setIsEmployed($experience['is_employed']);

            array_push($result, $jobExperience);
        }



        return $result;
    }

    public function updateJobExperience(array $experiences): array
    {
        $result = [];

        foreach ($experiences as $experience){
            if (isset($experience['id'])){
                $jobExperience = $this->_em->getRepository(JobExperience::class)->find($experience['id']);
                $jobExperience->setStartDate(\DateTime::createFromFormat('d.m.Y', $experience['start_date']));
                $jobExperience->setEndDate(\DateTime::createFromFormat('d.m.Y', $experience['end_date']));
                $jobExperience->setCity($experience['city']);
                $jobExperience->setCompany($experience['company']);
                $jobExperience->setIsEmployed($experience['is_employed']);
            }else{
                $jobExperience = new JobExperience();
                $jobExperience->setStartDate(\DateTime::createFromFormat('d.m.Y', $experience['start_date']));
                $jobExperience->setEndDate(\DateTime::createFromFormat('d.m.Y', $experience['end_date']));
                $jobExperience->setCity($experience['city']);
                $jobExperience->setCompany($experience['company']);
                $jobExperience->setIsEmployed($experience['is_employed']);
            }
            array_push($result, $jobExperience);
        }



        return $result;
    }

    public function searchAllJobExperience(array $experiences): int
    {
        return count($experiences);
    }

    public function getJobExperiences(array $experiences): array
    {
        $data = [];

        foreach ($experiences as $experience){
            $data[] = [
                'id' => $experience->getId(),
                'start_date' => $experience->getStartDate()->format('Y'),
                'end_date' => $experience->getEndDate()->format('Y'),
                'city' => $experience->getCity(),
                'company' => $experience->getCompany(),
                'is_employed' => $experience->getIsEmployed(),
                'description' => $experience->getDescription(),
                'position' => $experience->getPosition()
            ];
        }

        return $data;
    }
}
