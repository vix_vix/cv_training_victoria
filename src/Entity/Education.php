<?php

namespace App\Entity;

use App\Repository\EducationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EducationRepository::class)
 */
class Education
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $degreeName;

    /**
     * @ORM\Column(type="date")
     */
    private $graduationDate;

    /**
     * @ORM\ManyToOne(targetEntity=PersonalData::class, inversedBy="education", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $personalData;

    /**
     * @ORM\ManyToOne(targetEntity=DegreeType::class, inversedBy="education", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $speciality;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Faculty;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDegreeName(): ?string
    {
        return $this->degreeName;
    }

    public function setDegreeName(string $degreeName): self
    {
        $this->degreeName = $degreeName;

        return $this;
    }

    public function getGraduationDate(): ?\DateTimeInterface
    {
        return $this->graduationDate;
    }

    public function setGraduationDate(\DateTimeInterface $graduationDate): self
    {
        $this->graduationDate = $graduationDate;

        return $this;
    }

    public function getPersonalData(): ?PersonalData
    {
        return $this->personalData;
    }

    public function setPersonalData(?PersonalData $personalData): self
    {
        $this->personalData = $personalData;

        return $this;
    }

    public function getType(): ?DegreeType
    {
        return $this->type;
    }

    public function setType(?DegreeType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSpeciality(): ?string
    {
        return $this->speciality;
    }

    public function setSpeciality(string $speciality): self
    {
        $this->speciality = $speciality;

        return $this;
    }

    public function getFaculty(): ?string
    {
        return $this->Faculty;
    }

    public function setFaculty(string $Faculty): self
    {
        $this->Faculty = $Faculty;

        return $this;
    }
}
