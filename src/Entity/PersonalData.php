<?php

namespace App\Entity;

use App\Repository\PersonalDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PersonalDataRepository::class)
 */
class PersonalData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $profession;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible;

    /**
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @ORM\OneToMany(targetEntity=SocialLinks::class, mappedBy="personalData")
     */
    private $socialLinks;

    /**
     * @ORM\OneToMany(targetEntity=JobExperience::class, mappedBy="personalData")
     */
    private $jobExperiences;

    /**
     * @ORM\OneToMany(targetEntity=Education::class, mappedBy="personalData")
     */
    private $education;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sent_invitation;

    /**
     * @ORM\Column(type="text")
     */
    private $about;

    public function __toString()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function __construct()
    {
        $this->socialLinks = new ArrayCollection();
        $this->jobExperiences = new ArrayCollection();
        $this->education = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setProfession(string $profession): self
    {
        $this->profession = $profession;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return Collection<int, SocialLinks>
     */
    public function getSocialLinks(): Collection
    {
        return $this->socialLinks;
    }

    /**
     * @param SocialLinks $socialLink
     * @return $this
     */
    public function addSocialLink(SocialLinks $socialLink): self
    {
        if (!$this->socialLinks->contains($socialLink)) {
            $this->socialLinks[] = $socialLink;
            $socialLink->setPersonalData($this);
        }

        return $this;
    }

    public function removeSocialLink(SocialLinks $socialLink): self
    {
        if ($this->socialLinks->removeElement($socialLink)) {
            // set the owning side to null (unless already changed)
            if ($socialLink->getPersonalData() === $this) {
                $socialLink->setPersonalData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, JobExperience>
     */
    public function getJobExperiences(): Collection
    {
        return $this->jobExperiences;
    }

    public function addJobExperience(JobExperience $jobExperience): self
    {
        if (!$this->jobExperiences->contains($jobExperience)) {
            $this->jobExperiences[] = $jobExperience;
            $jobExperience->setPersonalData($this);
        }

        return $this;
    }

    public function removeJobExperience(JobExperience $jobExperience): self
    {
        if ($this->jobExperiences->removeElement($jobExperience)) {
            // set the owning side to null (unless already changed)
            if ($jobExperience->getPersonalData() === $this) {
                $jobExperience->setPersonalData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Education>
     */
    public function getEducation(): Collection
    {
        return $this->education;
    }

    public function addEducation(Education $education): self
    {
        if (!$this->education->contains($education)) {
            $this->education[] = $education;
            $education->setPersonalData($this);
        }

        return $this;
    }

    public function removeEducation(Education $education): self
    {
        if ($this->education->removeElement($education)) {
            // set the owning side to null (unless already changed)
            if ($education->getPersonalData() === $this) {
                $education->setPersonalData(null);
            }
        }

        return $this;
    }

    public function getSentInvitation(): ?bool
    {
        return $this->sent_invitation;
    }

    public function setSentInvitation(bool $sent_invitation): self
    {
        $this->sent_invitation = $sent_invitation;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(string $about): self
    {
        $this->about = $about;

        return $this;
    }
}
