<?php

namespace App\Command;

use App\Repository\PersonalDataRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetActive extends Command
{
    protected static $defaultName = 'app:set-inactive';
    protected static $defaultDescription = 'Make a user inactive';

    public function __construct(ManagerRegistry $em)
    {
        $this->entityManager = $em;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('id', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $id = (int)$input->getArgument('id');
        $entityManager = $this->entityManager->getManager();

        if($id){
            $personalDataRepository = new PersonalDataRepository($this->entityManager);
            $personalData = $personalDataRepository->find($id);

            if($personalData->getActive()){
                $personalData->setActive(0);

                $entityManager->persist($personalData);
                $entityManager->flush($personalData);

                $output->writeln('success');
            } else{
                $output->writeln('User is inactive');
            }
        }



        return Command::SUCCESS;

    }

}