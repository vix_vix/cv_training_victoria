<?php

namespace App\Command;

use App\Controller\CvController;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\EducationRepository;
use App\Repository\JobExperienceRepository;
use App\Repository\PersonalDataRepository;
use App\Repository\SocialLinksRepository;
use Doctrine\ORM\Mapping\Entity;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use  Doctrine\Persistence\ManagerRegistry;



class CreateNCv extends Command
{
    protected static $defaultName = 'app:create-n-records';
    protected static $defaultDescription = 'Creates new records.';

    private $entityManager;

    public function __construct(ManagerRegistry $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }
    protected function configure(): void
    {
        $this
            ->addArgument('records', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $testData = [
            "personal_data" =>  [
        "first_name" => "ALex2",
    "last_name" => "Seul",
    "email" => "alexfio2334@gmail.com",
    "phone" => "+37367386609",
    "address" => "Moldova",
    "profession" => "Front end",
    "active" => true,
    "visible" => false,
    "birthday" => "01.11.2009",
  ],
  "social_links" => [
      "www.mmm.com",
     "www.instagram.com",
     "www.facebook.com"
  ],
  "job_experience" => [
      [
        "start_date" => "04.02.2021",
      "end_date" => "09.08.2022",
      "city" => "Siegen",
      "company" => "Google",
      "is_employed" => true
    ],
      [
        "start_date" => "01.02.2021",
      "end_date" => "09.08.2022",
      "city" => "Berlin",
      "company" => "Facebook",
      "is_employed" => true
    ],
    [
        "start_date" => "01.02.2021",
      "end_date" => "09.08.2022",
      "city" => "Amzaon",
      "company" => "Moldova",
      "is_employed" => true
    ]
  ],
  "education" => [
      [
        "degree_name" => "MI",
      "graduation_date" => "09.09.2009",
      "id_type" => 3
    ],
    [
        "degree_name" => "MI",
      "graduation_date" => "10.09.2010",
      "id_type" => 2
    ],
     [
        "degree_name" => "AI",
      "graduation_date" => "19.09.2011",
      "id_type" => 2
    ]
  ]
];

        $records = (int)$input->getArgument('records');

        for ($i = 0; $i < $records; $i++){
            $socialLinksRepository = new SocialLinksRepository($this->entityManager);
            $socialLinks = $socialLinksRepository->addSocialLinks($testData['social_links']);

            $educationRepository = new EducationRepository($this->entityManager);
            $education = $educationRepository->addEducation($testData['education']);

            $jobExperienceRepository = new JobExperienceRepository($this->entityManager);
            $jobExperience = $jobExperienceRepository->addJobExperience($testData['job_experience']);

            $personalDataRepository = new PersonalDataRepository($this->entityManager);
            $personalDataRepository->addPersonalData($testData['personal_data'], $socialLinks, $education, $jobExperience);

        }

        $output->writeln('success');

        return Command::SUCCESS;

    }

}