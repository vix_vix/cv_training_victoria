<?php

namespace App\Command;

use App\Repository\PersonalDataRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Client\ClientInterface;

class CronSendEmail extends Command
{
    protected static $defaultName = 'app:send-email';

    private $client;
    private $em;

    public function __construct(ClientInterface $client, ManagerRegistry $em)
    {
        $this->client = $client;
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('It sends email to users which are active and visible.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $entityManager =$this->em;
        $personalDataRepository = new PersonalDataRepository($entityManager);

        $personalData = $personalDataRepository->findAll();

        foreach ($personalData as $person){
            if ($person->getActive() && $person->getVisible() && !$person->getSentInvitation()){

                $id = $person->getId();

                $person->setSentInvitation(1);
                $entityManager->getManager()->persist($person);
                $entityManager->getManager()->flush($person);

                $request = $this->client->createRequest('GET', "http://fiodorov-cv.loc/api/cv/sendEmail/$id/en");
                $this->client->sendRequest($request);
            }
        }

        return 0;
    }

}