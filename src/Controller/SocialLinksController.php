<?php

namespace App\Controller;

use App\Entity\SocialLinks;
use App\Form\SocialLinksType;
use App\Repository\SocialLinksRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/social/links")
 */
class SocialLinksController extends AbstractController
{
    /**
     * @Route("/", name="app_social_links_index", methods={"GET"})
     */
    public function index(SocialLinksRepository $socialLinksRepository): Response
    {
        return $this->render('social_links/index.html.twig', [
            'social_links' => $socialLinksRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_social_links_new", methods={"GET", "POST"})
     */
    public function new(Request $request, SocialLinksRepository $socialLinksRepository): Response
    {
        $socialLink = new SocialLinks();
        $form = $this->createForm(SocialLinksType::class, $socialLink);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $socialLinksRepository->add($socialLink);
            return $this->redirectToRoute('app_social_links_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('social_links/new.html.twig', [
            'social_link' => $socialLink,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_social_links_show", methods={"GET"})
     */
    public function show(SocialLinks $socialLink): Response
    {
        return $this->render('social_links/show.html.twig', [
            'social_link' => $socialLink,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_social_links_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, SocialLinks $socialLink, SocialLinksRepository $socialLinksRepository): Response
    {
        $form = $this->createForm(SocialLinksType::class, $socialLink);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $socialLinksRepository->add($socialLink);
            return $this->redirectToRoute('app_social_links_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('social_links/edit.html.twig', [
            'social_link' => $socialLink,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_social_links_delete", methods={"POST"})
     */
    public function delete(Request $request, SocialLinks $socialLink, SocialLinksRepository $socialLinksRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$socialLink->getId(), $request->request->get('_token'))) {
            $socialLinksRepository->remove($socialLink);
        }

        return $this->redirectToRoute('app_social_links_index', [], Response::HTTP_SEE_OTHER);
    }
}
