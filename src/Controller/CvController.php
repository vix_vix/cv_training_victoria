<?php

namespace App\Controller;


use App\Entity\SocialLinks;
use App\Repository\EducationRepository;
use App\Repository\JobExperienceRepository;
use App\Repository\PersonalDataRepository;
use App\Repository\SocialLinksRepository;
use App\Service\EmailSenderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;


/**
 * @Route("/api/cv", name="cv")
 */
class CvController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request): Response
    {

        return $this->render('manage.html.twig');
    }

    /**
     * @Route("/create", name="create_cv", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        $entityManager = $this->getDoctrine();
        $data = json_decode($request->getContent(), true);
        $validation = $this->validateRequest($data)->getContent();

        if ($validation === 'success') {

                $socialLinksRepository = new SocialLinksRepository($entityManager);
                $socialLinks = $socialLinksRepository->addSocialLinks($data['social_links']);

                $educationRepository = new EducationRepository($entityManager);
                $education = $educationRepository->addEducation($data['education']);

                $jobExperienceRepository = new JobExperienceRepository($entityManager);
                $jobExperience = $jobExperienceRepository->addJobExperience($data['job_experience']);

                $personalDataRepository = new PersonalDataRepository($entityManager);
                $personalDataRepository->addPersonalData($data['personal_data'], $socialLinks, $education, $jobExperience);

        } else {
            return new Response('Data is not valid');
        }

        return new Response('Cv is created',Response::HTTP_OK);
    }

    /**
     * @Route("/update/{id}", name="update_cv", methods={"PATCH"})
     */
    public function update(Request $request, int $id): Response
    {
        $entityManager = $this->getDoctrine();
        $data = json_decode($request->getContent(), true);
        $validation = $this->validateRequest($data, $id)->getContent();

        if ($id && $validation === 'success') {

            $socialLinksRepository = new SocialLinksRepository($entityManager);
            $socialLinks = $socialLinksRepository->updateSocialLinks($data['social_links']);

            $educationRepository = new EducationRepository($entityManager);
            $education = $educationRepository->updateEducation($data['education']);

            $jobExperienceRepository = new JobExperienceRepository($entityManager);
            $jobExperience = $jobExperienceRepository->updateJobExperience($data['job_experience']);

            $personalDataRepository = new PersonalDataRepository($entityManager);
            $personalDataRepository->updatePersonalData($data['personal_data'], $socialLinks, $education, $jobExperience, $id);


        } else {
            return new Response("empty id or invalid data");
        }

        return new Response('Data is updated',Response::HTTP_OK);
    }

    /**
     * @Route("/searchAll", name="search_all_cv", methods={"GET"})
     */
    public function searchAll(): Response
    {
        $entityManager = $this->getDoctrine();

        $personalDataRepository = new PersonalDataRepository($entityManager);
        $allPersonalData = $personalDataRepository->searchAllPersonalData($entityManager);

        return $this->json($allPersonalData);
    }

    /**
     * @Route("/searchOne/{id}", name="search_one_cv", methods={"GET"})
     */
    public function searchOne(int $id): Response
    {
        $entityManager = $this->getDoctrine();

        $personalDataRepository = new PersonalDataRepository($entityManager);
        $response = $personalDataRepository->searchOnePersonalData($id, $entityManager);

        return $this->json($response);
    }

    /**
     * @Route("/searchOneQueryBuilder/{id}", name="search_all_query_builder_cv", methods={"GET"})
     */
    public function searchOneQueryBuilder(int $id): Response
    {
        $entityManager = $this->getDoctrine();
        $personalDataRepository = new PersonalDataRepository($entityManager);
        $personalData = $personalDataRepository->searchOneQueryBuilder($id);

        return $this->json($personalData);
    }

    public function checkRequestType(int $id = 0): array
    {
        $dateFormat = 'd.m.Y';

        if ($id > 0){
            return [
                'personal_data' => [
                    new Assert\Collection([
                        'first_name' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'last_name'=> [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'email' => [new Assert\Email(), new Assert\Length(['max' => 255])],
                        'phone' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'address' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'profession' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'active' => new Assert\Type('bool'),
                        'visible' => new Assert\Type('bool'),
                        'birthday' => new Assert\DateTime($dateFormat)
                    ])

                ],
                'social_link' => new Assert\Collection([
                    'id' => new Assert\Type("int"),
                    'link' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                ]),
                'job_experience' => [
                    new Assert\Collection([
                        'id' => new Assert\Type("int"),
                        'start_date' => new Assert\DateTime($dateFormat),
                        'end_date'  => new Assert\DateTime($dateFormat),
                        'city' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'company' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'is_employed' => new Assert\Type('bool')
                    ])
                ],
                'education' => [
                    new Assert\Collection([
                        'id' => new Assert\Type("int"),
                        'degree_name' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                        'graduation_date' => new Assert\DateTime($dateFormat),
                        'id_type' => [new Assert\Type("int"), new Assert\Choice([1,2,3])]
                    ])
                ]
            ];
        }

        return [
            'personal_data' => [
                new Assert\Collection([
                    'first_name' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'last_name'=> [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'email' => [new Assert\Email(), new Assert\Length(['max' => 255])],
                    'phone' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'address' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'profession' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'active' => new Assert\Type('bool'),
                    'visible' => new Assert\Type('bool'),
                    'birthday' => new Assert\DateTime($dateFormat)
                ])

            ],
            'social_link' => [new Assert\Collection([0 => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])]])],
            'job_experience' => [
                new Assert\Collection([
                    'start_date' => new Assert\DateTime($dateFormat),
                    'end_date'  => new Assert\DateTime($dateFormat),
                    'city' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'company' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'is_employed' => new Assert\Type('bool')
                ])
            ],
            'education' => [
                new Assert\Collection([
                    'degree_name' => [new Assert\Type("string"), new Assert\Length(['min' => 1]), new Assert\Length(['max' => 255])],
                    'graduation_date' => new Assert\DateTime($dateFormat),
                    'id_type' => [new Assert\Type("int"), new Assert\Choice([1,2,3])]
                ])
            ]
        ];
    }

    public function validateRequest(array $data, int $id = 0): Response
    {
        $validator = Validation::createValidator();
        $constraint  = $this->checkRequestType($id);
        $errors = [];

        if (count($validator->validate($data['personal_data'], $constraint['personal_data'])) > 0){
            $errors[] = [
                'personal_data_errors' => $validator->validate($data['personal_data'], $constraint['personal_data'])
            ];
        }

        foreach ($data['social_links'] as $index => $link){
            if(is_string($link)){
                if (count($validator->validate([$link], $constraint['social_link'])) > 0){
                    $errors['social_links_errors'] = [
                        "link $index" =>$validator->validate([$link], $constraint['social_link'])
                    ];
                }
            } else{
                if (count($validator->validate($link, $constraint['social_link'])) > 0){
                    $errors['social_links_errors'] = [
                        "link $index" =>$validator->validate([$link], $constraint['social_link'])
                    ];
                }
            }
        }
        foreach ($data['job_experience'] as $index => $job){
            if(count($validator->validate($job, $constraint['job_experience'])) > 0){
                $errors['job_experience_errors'] = [
                    "job $index" =>$validator->validate($job, $constraint['job_experience'])
                ];
            }

        }

        foreach ($data['education'] as $index => $education){
            if (count($validator->validate($education, $constraint['education'])) > 0){
                $errors['education_errors'] = [
                    "education $index" =>$validator->validate($education, $constraint['education'])
                ];
            }

        }
        if (!empty($errors)) {
            return $this->json($errors);
        }

        return new Response('success');
    }

    /**
     * @Route("/sendEmail/{id}/{language}", name="send_email_cv", requirements={"_locale": "en|fr"})
     * @param int $id
     * @param string $language
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendEmail(Request $request, MailerInterface $mailer, $appEmail, $publicDir, EmailSenderService $emailService, int $id, string $language): Response
    {
        $entityManager = $this->getDoctrine();
        $personalDataRepository = new PersonalDataRepository($entityManager);
        $personData = $personalDataRepository->find($id);

        if (!$personData){
            throw new NotFoundHttpException('Not found person');
        }

        $emailService->emailTemplate($request,$mailer, $appEmail, $publicDir, $personData->getEmail(), $language);
        return new Response('Email sent');
    }
}