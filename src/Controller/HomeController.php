<?php

namespace App\Controller;


use App\Repository\PersonalDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home")
 */
class HomeController extends AbstractController
{

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $entityManager = $this->getDoctrine();

        $personalDataRepository = new PersonalDataRepository($entityManager);
        $response = $personalDataRepository->searchOnePersonalData(1, $entityManager);

        return $this->render('manage.html.twig', $response);
    }
}