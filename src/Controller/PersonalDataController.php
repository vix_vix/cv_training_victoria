<?php

namespace App\Controller;

use App\Entity\PersonalData;
use App\Form\PersonalDataType;
use App\Repository\PersonalDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/personal/data")
 */
class PersonalDataController extends AbstractController
{
    /**
     * @Route("/", name="app_personal_data_index", methods={"GET"})
     */
    public function index(PersonalDataRepository $personalDataRepository): Response
    {
        return $this->render('personal_data/index.html.twig', [
            'personal_datas' => $personalDataRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_personal_data_new", methods={"GET", "POST"})
     */
    public function new(Request $request, PersonalDataRepository $personalDataRepository): Response
    {
        $personalDatum = new PersonalData();
        $form = $this->createForm(PersonalDataType::class, $personalDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $personalDataRepository->add($personalDatum);
            return $this->redirectToRoute('app_personal_data_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('personal_data/new.html.twig', [
            'personal_datum' => $personalDatum,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_personal_data_show", methods={"GET"})
     */
    public function show(PersonalData $personalDatum): Response
    {
        return $this->render('personal_data/show.html.twig', [
            'personal_datum' => $personalDatum,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_personal_data_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, PersonalData $personalDatum, PersonalDataRepository $personalDataRepository): Response
    {
        $form = $this->createForm(PersonalDataType::class, $personalDatum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $personalDataRepository->add($personalDatum);
            return $this->redirectToRoute('app_personal_data_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('personal_data/edit.html.twig', [
            'personal_datum' => $personalDatum,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_personal_data_delete", methods={"POST"})
     */
    public function delete(Request $request, PersonalData $personalDatum, PersonalDataRepository $personalDataRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$personalDatum->getId(), $request->request->get('_token'))) {
            $personalDataRepository->remove($personalDatum);
        }

        return $this->redirectToRoute('app_personal_data_index', [], Response::HTTP_SEE_OTHER);
    }
}
