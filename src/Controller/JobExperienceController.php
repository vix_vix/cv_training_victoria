<?php

namespace App\Controller;

use App\Entity\JobExperience;
use App\Form\JobExperienceType;
use App\Repository\JobExperienceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/job/experience")
 */
class JobExperienceController extends AbstractController
{
    /**
     * @Route("/", name="app_job_experience_index", methods={"GET"})
     */
    public function index(JobExperienceRepository $jobExperienceRepository): Response
    {
        return $this->render('job_experience/index.html.twig', [
            'job_experiences' => $jobExperienceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_job_experience_new", methods={"GET", "POST"})
     */
    public function new(Request $request, JobExperienceRepository $jobExperienceRepository): Response
    {
        $jobExperience = new JobExperience();
        $form = $this->createForm(JobExperienceType::class, $jobExperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $jobExperienceRepository->add($jobExperience);
            return $this->redirectToRoute('app_job_experience_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('job_experience/new.html.twig', [
            'job_experience' => $jobExperience,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_job_experience_show", methods={"GET"})
     */
    public function show(JobExperience $jobExperience): Response
    {
        return $this->render('job_experience/show.html.twig', [
            'job_experience' => $jobExperience,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_job_experience_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, JobExperience $jobExperience, JobExperienceRepository $jobExperienceRepository): Response
    {
        $form = $this->createForm(JobExperienceType::class, $jobExperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $jobExperienceRepository->add($jobExperience);
            return $this->redirectToRoute('app_job_experience_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('job_experience/edit.html.twig', [
            'job_experience' => $jobExperience,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_job_experience_delete", methods={"POST"})
     */
    public function delete(Request $request, JobExperience $jobExperience, JobExperienceRepository $jobExperienceRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$jobExperience->getId(), $request->request->get('_token'))) {
            $jobExperienceRepository->remove($jobExperience);
        }

        return $this->redirectToRoute('app_job_experience_index', [], Response::HTTP_SEE_OTHER);
    }
}
