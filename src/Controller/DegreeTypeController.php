<?php

namespace App\Controller;

use App\Entity\DegreeType;
use App\Form\DegreeTypeType;
use App\Repository\DegreeTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/degree/type")
 */
class DegreeTypeController extends AbstractController
{
    /**
     * @Route("/", name="app_degree_type_index", methods={"GET"})
     */
    public function index(DegreeTypeRepository $degreeTypeRepository): Response
    {
        return $this->render('degree_type/index.html.twig', [
            'degree_types' => $degreeTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_degree_type_new", methods={"GET", "POST"})
     */
    public function new(Request $request, DegreeTypeRepository $degreeTypeRepository): Response
    {
        $degreeType = new DegreeType();
        $form = $this->createForm(DegreeTypeType::class, $degreeType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $degreeTypeRepository->add($degreeType);
            return $this->redirectToRoute('app_degree_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('degree_type/new.html.twig', [
            'degree_type' => $degreeType,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_degree_type_show", methods={"GET"})
     */
    public function show(DegreeType $degreeType): Response
    {
        return $this->render('degree_type/show.html.twig', [
            'degree_type' => $degreeType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_degree_type_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, DegreeType $degreeType, DegreeTypeRepository $degreeTypeRepository): Response
    {
        $form = $this->createForm(DegreeTypeType::class, $degreeType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $degreeTypeRepository->add($degreeType);
            return $this->redirectToRoute('app_degree_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('degree_type/edit.html.twig', [
            'degree_type' => $degreeType,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_degree_type_delete", methods={"POST"})
     */
    public function delete(Request $request, DegreeType $degreeType, DegreeTypeRepository $degreeTypeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$degreeType->getId(), $request->request->get('_token'))) {
            $degreeTypeRepository->remove($degreeType);
        }

        return $this->redirectToRoute('app_degree_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
