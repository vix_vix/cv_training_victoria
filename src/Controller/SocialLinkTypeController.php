<?php

namespace App\Controller;

use App\Entity\SocialLinkType;
use App\Form\SocialLinkTypeType;
use App\Repository\SocialLinkTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/social/link/type")
 */
class SocialLinkTypeController extends AbstractController
{
    /**
     * @Route("/", name="app_social_link_type_index", methods={"GET"})
     */
    public function index(SocialLinkTypeRepository $socialLinkTypeRepository): Response
    {
        return $this->render('social_link_type/index.html.twig', [
            'social_link_types' => $socialLinkTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_social_link_type_new", methods={"GET", "POST"})
     */
    public function new(Request $request, SocialLinkTypeRepository $socialLinkTypeRepository): Response
    {
        $socialLinkType = new SocialLinkType();
        $form = $this->createForm(SocialLinkTypeType::class, $socialLinkType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $socialLinkTypeRepository->add($socialLinkType);
            return $this->redirectToRoute('app_social_link_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('social_link_type/new.html.twig', [
            'social_link_type' => $socialLinkType,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_social_link_type_show", methods={"GET"})
     */
    public function show(SocialLinkType $socialLinkType): Response
    {
        return $this->render('social_link_type/show.html.twig', [
            'social_link_type' => $socialLinkType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_social_link_type_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, SocialLinkType $socialLinkType, SocialLinkTypeRepository $socialLinkTypeRepository): Response
    {
        $form = $this->createForm(SocialLinkTypeType::class, $socialLinkType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $socialLinkTypeRepository->add($socialLinkType);
            return $this->redirectToRoute('app_social_link_type_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('social_link_type/edit.html.twig', [
            'social_link_type' => $socialLinkType,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_social_link_type_delete", methods={"POST"})
     */
    public function delete(Request $request, SocialLinkType $socialLinkType, SocialLinkTypeRepository $socialLinkTypeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$socialLinkType->getId(), $request->request->get('_token'))) {
            $socialLinkTypeRepository->remove($socialLinkType);
        }

        return $this->redirectToRoute('app_social_link_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
