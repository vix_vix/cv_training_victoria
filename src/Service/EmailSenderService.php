<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class EmailSenderService
{
    /**
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function emailTemplate(Request $request,MailerInterface $mailer, $appEmail, $publicDir, string $userEmail, string $language): bool
    {
        $request->setLocale($language);

        $email = (new TemplatedEmail())
            ->from($appEmail)
            ->to(new Address($userEmail, 'Alex'))
            ->subject('Success')
            ->htmlTemplate('emails/email.html.twig')
            ->attachFromPath($publicDir . '/png/form.png')
            ->context([
                'user_locale' => $request->getLocale()
            ]);

        $mailer->send($email);

        return true;
    }


}